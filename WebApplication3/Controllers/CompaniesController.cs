﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiGrTech.Models;
using WebApiGrTech.Repository;
using WebApiGrTech.Authentications;

namespace WebApiGrTech.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = Role.Admin)]
    public class CompaniesController : ControllerBase
    {
        private readonly GrTechDataContext _context;

        public CompaniesController(GrTechDataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetCompanies()
        {
            var companies = await _context.Companies.ToListAsync();
            return Ok(companies);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCompany(int id)
        {
            var company = await _context.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound("Data not found");
            }

            return Ok(company);
        }

        [HttpPost]
        public async Task<IActionResult> AddCompany([FromBody] Company company)
        {
            if (ModelState.IsValid)
            {
                var exist_company = _context.Companies.SingleOrDefault(m => m.Name == company.Name);
                if (exist_company != null)
                {
                    return BadRequest("Company already exist! Can't add same company");
                }

                await _context.AddAsync<Company>(company);
                int row_added = await _context.SaveChangesAsync();

                if (row_added > 0)
                    return Ok("Add company successfully");
                else return BadRequest("Add Company failed");
            }

            return BadRequest("Request data invalid");
        }

        // DELETE: api/Companies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCompany(int id)
        {
            var company = await _context.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound("Data not found");
            }

            _context.Companies.Remove(company);
            await _context.SaveChangesAsync();

            return Ok("Delete Company successfully");
        }

    }
}
