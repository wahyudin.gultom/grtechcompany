﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiGrTech.Response;
using WebApiGrTech.Repository;
using Microsoft.AspNetCore.Authorization;

namespace WebApiGrTech.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly GrTechDataContext grTechDataContext;
        public AccountController(GrTechDataContext grTechDataContext)
        {
            this.grTechDataContext = grTechDataContext;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginRequest req)
        {
            var user = grTechDataContext.Users.SingleOrDefault(x => x.Email == req.UserName);
            if (user != null)
            {
                if (user.Password.Equals(req.Password))
                {
                    return Ok("Login successfully");
                }
            }
            return BadRequest("Username or Password is invalid");
        }
    }
}
