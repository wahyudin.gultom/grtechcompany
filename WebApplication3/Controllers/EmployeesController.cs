﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiGrTech.Models;
using WebApiGrTech.Repository;
using WebApiGrTech.Authentications;
using Microsoft.AspNetCore.Authorization;

namespace WebApiGrTech.Controllers
{
    [Authorize(Roles = Role.Admin)]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly GrTechDataContext _context;

        public EmployeesController(GrTechDataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetEmployees()
        {
            var employees = await _context.Employees.ToListAsync();
            return Ok(employees);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmployee(int id)
        {
            var employee = await _context.Employees.FindAsync(id);

            if (employee == null)
            {
                return NotFound("Data not found");
            }

            return Ok(employee);
        }

        [HttpPost]
        public async Task<IActionResult> PostEmployee([FromBody] Employee employee)
        {
            if (ModelState.IsValid)
            {
                var exist_employee = _context.Employees.SingleOrDefault(m => m.FirstName == employee.FirstName && m.LastName == employee.LastName);
                if (exist_employee != null)
                {
                    return BadRequest("Employee already exist! Can't add same employee");
                }

                await _context.AddAsync<Employee>(employee);
                int row_added = await _context.SaveChangesAsync();

                if (row_added > 0)
                    return Ok("Add Employee successfully");
                else return BadRequest("Add Employee failed");
            }

            return BadRequest("Request data invalid");
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(int id)
        {
            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound("data not found");
            }

            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();

            return Ok("Delete Employee successfully");
        }
    }
}
