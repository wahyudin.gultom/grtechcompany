﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApiGrTech.Response
{
    public class LoginRequest
    {
        [Required(ErrorMessage = "Username can't empty!")]
        public string UserName { get; set; }
        
        [Required(ErrorMessage = "Password can't empty!")]
        public string Password { get; set; }
    }
}
