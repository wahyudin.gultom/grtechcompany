﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiGrTech.Models;

namespace WebApiGrTech.Repository
{
    public class GrTechDataContext:DbContext
    {
        public GrTechDataContext(DbContextOptions options):base(options)
        {

        }

        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<User> Users { get; set; }

    }
}
