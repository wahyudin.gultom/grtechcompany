﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiGrTech.Models
{
    public class Company
    {
        public int CompanyID { get; set; }
        public string Name { get; set; }
        public string ImageURL { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
    }
}
